<?php

require("pdo.php");



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <title>Vrabotuvanje Studenti</title>
</head>
<style>
    form.bg-warning.pt-2.pb-3 {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
}
</style>
<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-warning shadow p-2 rounded">
        <a href="index.html"><img class="img-fluid" src="images/logo.png" alt="brainster"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">   
                <i class="fas fa-align-right"></i>
            </span>
        </button>

        <div class="collapse navbar-collapse navcolapsedcss" id="navbarSupportedContent">
            <ul class="navbar-nav m-auto responsive-nav">
                <li class="nav-item"><a class="nav-link" href="https://www.manutd.com/" target="_blank">Академија за маркетинг</a></li>
                <li class="nav-item"><a class="nav-link" href=" http://codepreneurs.co/">Академија за програмирање</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Академија за data science</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Академија за дизајн</a></li>
            </ul>

            <a href="#"><button type="button" class="btn btn-danger btn-sm">Вработи наш студент</button></a>
        </div>
    </nav>
    <div class="main bg-warning pt-3  pb-5">
        <h5 class=" text-center mt-5 mb-5 mr-5 font-weight-bold">ВРАБОТИ СТУДЕНТИ</h5>
    </div>

    <form class="bg-warning pt-2 pb-3" action="test.php" method="POST">
        <div class="row ">
            <div class="col-md-4 offset-md-2 col-sm-12">
                <label for="name">Име и презиме</label>
                <input type="text" class="form-control" placeholder="Вашето име и презиме" name="name" id="name">
                <?php if(isset($_GET["NameEmpty"])){ ?>
                <span style="color: red;">Вашето име е потребно!</span>
                <?php } ?>
            </div>
            <div class="col-md-4 col-sm-12">
                <label for="companyName">Име на компанија</label>
                <input type="text" class="form-control" placeholder="Име на вашата компанија" name="companyName" id="companyName">
                <?php if(isset($_GET["NameEmpty"])){ ?>
                <span style="color: red;">Име на компанија е потребно!</span>
                <?php } ?>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-4 offset-md-2 col-sm-12">
                <label for="email">Контакт имејл</label>
                <input type="" class="form-control" id="email" placeholder="Контакт имејл на вашата компанија" name="email" id="email">
                <?php if(isset($_GET["EmailEmpty"])){ ?> 
                <span style="color: red;">Email is required!</span>
                <?php } ?>
                <?php if(isset($_GET["EmailNotValidate"])){ ?>
                <span style="color: red;">Email is not valid!</span>
                <?php } ?>
            </div>
            <div class="col-md-4 col-sm-12">
                <label for="tel">Контакт телефон</label>
                <input type="tel" class="form-control" placeholder="Контакт телефон на вашата компанија" name="tel" id="tel">
                <?php if(isset($_GET["telEmpty"])){ ?>
                <span style="color: red;">Nummber is required!</span>
                <?php } ?>
                <?php if(isset($_GET["telError"])){ ?>
                <span style="color: red;">Nummber is not valid!</span>
                <?php } ?>
            </div>
        </div>

        <?php 
        $query = $conn->query("SELECT * FROM `typestudent` ");
        $studenti = $query->fetchAll();
        ?>
        <div class="row mt-3 pb-1">
            <div class="col-md-4 offset-md-2 col-sm-12">
                <div class="form-group">
                    <label for="typeStudent">Тип на студент:</label>
                    <select class="form-control" id="typeStudent" name="typeStudent">
                                        <option value="">Изберете тип на студент</option>
                                       
                                         <?php
                                        foreach($studenti as $student){
                                            echo "<option value='{$student['id']}'>{$student['student_type']}</option>";
                                        }
                                        ?> -->
                                       
                    </select>
                    <?php if(isset($_GET["typeStudentNotValidate"])){ ?>
                <span style="color: red;">Select option is not choosen!</span>
                <?php } ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <button type="submit" name="" class="btn btn-danger btn-lg mt-4 col-md-12 col-sm-12 isprati">Испрати</button>

            </div>
        </div>
    </form>


    <footer class="page-footer font-small mdb-color darken-3 pt-4 pb-4 text-white bg-dark" id="foot">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-6">
                    <div class="footer-copyright text-center py-3">© 2020 Copyright:
                        <small class="m-auto">Izraboteno so <i class="fa fa-heart" aria-hidden="true"></i> od studentite na brainster</small>
                    </div>
                </div>
            </div>
        </div>

    </footer>
</body>

</html>