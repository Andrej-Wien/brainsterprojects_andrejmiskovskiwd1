-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 30, 2020 at 06:12 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `students`
--

-- --------------------------------------------------------

--
-- Table structure for table `typestudent`
--

CREATE TABLE `typestudent` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_type` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `typestudent`
--

INSERT INTO `typestudent` (`id`, `student_type`) VALUES
(1, 'Programer'),
(2, 'Marketing agent'),
(3, 'data scientist'),
(4, 'web dizajner');

-- --------------------------------------------------------

--
-- Table structure for table `vrabotuvanje`
--

CREATE TABLE `vrabotuvanje` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `companyName` varchar(64) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `tel` varchar(16) DEFAULT NULL,
  `typestudent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vrabotuvanje`
--

INSERT INTO `vrabotuvanje` (`id`, `name`, `companyName`, `email`, `tel`, `typestudent_id`, `created_at`) VALUES
(10, 'Andrej Miskovski', 'brainster', 'andrejmiskovski@yahoo.com', '1234', 1, '2020-11-30 00:21:02'),
(11, 'Andrej Miskovski', 'brainster', 'andrejmiskovski@yahoo.com', '1234', 1, '2020-11-30 00:25:30'),
(12, 'Andrej Miskovski', 'brainster', 'andrejmiskovski@yahoo.com', '1234', 1, '2020-11-30 00:25:45'),
(13, 'Andrej Miskovski', 'brainster', 'andrejmiskovski@yahoo.com', '070123456', 1, '2020-11-30 00:27:20'),
(14, 'Andrej Miskovski', 'brainster', 'andrejmiskovski@yahoo.com', '070123456', 1, '2020-11-30 00:31:48'),
(15, 'Andrej Miskovski', 'brainster', 'andrejmiskovski@yahoo.com', '070123987', 3, '2020-11-30 01:05:44'),
(16, 'Dejan Blazeski', 'brainster', 'deki@yahoo.com', '070123983', 1, '2020-11-30 02:25:17'),
(17, 'Dejan Blazeski', 'best solutions', 'deki@yahoo.com', '070123983', 4, '2020-11-30 02:30:41'),
(18, 'Vanesicka Oblakvovic', 'TEAM 19', 'vanessa.oblak@outlook.com', '+436767143533', 2, '2020-11-30 16:45:13'),
(19, 'Andrej Miskovski', 'brainster', 'vanessa.oblak@outlook.com', '0701234444', 1, '2020-11-30 18:06:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `typestudent`
--
ALTER TABLE `typestudent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vrabotuvanje`
--
ALTER TABLE `vrabotuvanje`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vrabotuvanje_location_id_to_typestudent_id` (`typestudent_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `typestudent`
--
ALTER TABLE `typestudent`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vrabotuvanje`
--
ALTER TABLE `vrabotuvanje`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `vrabotuvanje`
--
ALTER TABLE `vrabotuvanje`
  ADD CONSTRAINT `vrabotuvanje_location_id_to_typestudent_id` FOREIGN KEY (`typestudent_id`) REFERENCES `typestudent` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
