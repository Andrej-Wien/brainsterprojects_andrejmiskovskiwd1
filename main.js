document.querySelector(".filter_marketing").addEventListener("click", showMarketing);
document.querySelector(".filter_programing").addEventListener("click", showPrograming);
document.querySelector(".filter_design").addEventListener("click", showDesign);

function showMarketing() {
    // alert("hello");
    hideCards();

    var marketingCards = document.querySelectorAll(".marketing"); //niza od karticki
    for (var i = 0; i < marketingCards.length; i++) {
        marketingCards[i].style.display = "inline-block";
    }

}

function showPrograming() {

    hideCards();

    var programingCards = document.querySelectorAll(".programing"); //niza od karticki
    for (var i = 0; i < programingCards.length; i++) {
        programingCards[i].style.display = "inline-block";
    }

}

function showDesign() {

    hideCards();

    var designCards = document.querySelectorAll(".design"); //niza od karticki
    for (var i = 0; i < designCards.length; i++) {
        designCards[i].style.display = "inline-block";
    }

}

function hideCards() {
    var card = document.querySelectorAll(".card-container");

    for (var i = 0; i < card.length; i++) {
        card[i].style.display = "none";

    }
}